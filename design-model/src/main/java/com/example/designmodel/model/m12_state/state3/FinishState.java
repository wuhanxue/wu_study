package com.example.designmodel.model.m12_state.state3;

/**
 * @author benjamin_5
 * @Description
 * @date 2024/8/27
 */
public class FinishState extends State{

    @Override
    protected boolean handle(Long dataId) {
        System.out.printf("数据(id=%s)已销毁/出售，无法操作%n", dataId);
        return true;
    }

    @Override
    protected boolean handle(DataInfoOfflineContext context) {
        System.out.printf("数据(id=%s)已销毁/出售，无法操作%n", context.getDataId());
        return true;
    }

    @Override
    protected boolean handleAfter(DataInfoOfflineContext context) {
        return true;
    }
}
