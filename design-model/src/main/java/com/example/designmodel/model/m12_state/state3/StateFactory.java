package com.example.designmodel.model.m12_state.state3;

/**
 * @author benjamin_5
 * @Description
 * @date 2024/8/27
 */
public class StateFactory {
    public static State create(int state){
        State stateHandler = null;
        switch (state){
            case 1: stateHandler = new DownState();break;
            case 2: stateHandler = new DeleteState();break;
            case 3: stateHandler = new SaleState();break;
            default:
                throw new RuntimeException("状态值有误，请检查");
        }
        return stateHandler;
    }
}
