package com.example.designmodel.model.m12_state.state3;

/**
 * @author benjamin_5
 * @Description
 * @date 2024/8/27
 */
public abstract class State {
    public int STATE = 0;

    /**
     * 不传入context, 不涉及到环境中状态或其他变量值的更新，状态手动指定
     * @param dataId
     * @return
     */
    protected abstract boolean handle(Long dataId);

    /**
     * 传入context, 涉及到环境中状态或其他变量值的更新
     * @param context
     * @return
     */
    protected abstract boolean handle(DataInfoOfflineContext context);

    /**
     * 对应状态操作完成后的附件操作，如日志记录、资源删除等
     * @param context
     * @return
     */
    protected abstract boolean handleAfter(DataInfoOfflineContext context);

    /**
     * 结合模版方法模式，组合操作
     * @param context
     * @return
     */
    boolean call(DataInfoOfflineContext context){
        return handle(context) && handleAfter(context);
    }
}
