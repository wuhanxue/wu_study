package com.example.designmodel.model.m12_state.state3;

/**
 * @author benjamin_5
 * @Description
 * @date 2024/8/27
 */
public class DownState extends State{

    public int STATE = 1;

    @Override
    protected boolean handle(Long dataId) {
        System.out.printf("数据(id=%s)下架%n", dataId);
        return true;
    }

    @Override
    protected boolean handle(DataInfoOfflineContext context) {
        System.out.printf("数据(id=%s)下架%n", context.getDataId());
        context.setStateHandler(new DeleteState());
        return true;
    }

    @Override
    protected boolean handleAfter(DataInfoOfflineContext context) {
        System.out.printf("数据(id=%s)下架日志新增成功%n", context.getDataId());
        return true;
    }
}
