package com.example.designmodel.model.m12_state.state3;

/**
 * @author benjamin_5
 * @Description
 * @date 2024/8/27
 */
public class Test {

    public static void main(String[] args) {
        Long dataId = 1L;

        DataInfoOfflineContext context = new DataInfoOfflineContext();
        context.offline(dataId, 1);

        context.offline(dataId);

        context.offline(dataId, 3);

        context.offline(dataId);
    }
}
