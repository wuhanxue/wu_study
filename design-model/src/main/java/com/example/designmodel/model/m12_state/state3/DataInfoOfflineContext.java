package com.example.designmodel.model.m12_state.state3;

import lombok.Data;

/**
 * @author benjamin_5
 * @Description
 * @date 2024/8/27
 */
@Data
public class DataInfoOfflineContext {
    private State stateHandler;
    private Long dataId;

    public boolean offline(Long dataId, int state){
        // 根据状态判定调用的State类, 结合工厂模式
        this.dataId = dataId;
        this.stateHandler = StateFactory.create(state);
        return stateHandler.call(this);
//        return stateHandler.handle(dataId);
    }

    public boolean offline(Long dataId){
        // 根据状态判定调用的State类, 结合工厂模式
        this.dataId = dataId;
        return this.stateHandler.call(this);
    }
}
