package com.example.designmodel.model.m12_state.state3;

/**
 * @author benjamin_5
 * @Description
 * @date 2024/8/27
 */
public class SaleState extends State{

    public int STATE = 3;

    @Override
    protected boolean handle(Long dataId) {
        System.out.printf("数据(id=%s)出售%n", dataId);
        return true;
    }

    @Override
    protected boolean handle(DataInfoOfflineContext context) {
        System.out.printf("数据(id=%s)出售%n", context.getDataId());
        context.setStateHandler(new FinishState());
        return true;
    }

    @Override
    protected boolean handleAfter(DataInfoOfflineContext context) {
        System.out.printf("数据(id=%s)出售日志新增成功%n", context.getDataId());
        return true;
    }
}
