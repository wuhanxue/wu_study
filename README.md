# 1.项目简介

各类实战项目及工具集整合项目，附带源码详细思路介绍及原理分析。

# 2.文件结构简介
- demo：实战演示项目
- design-model: 设计模式
- tool: 通用工具包

# 3.demo
## assembly_plugin_demo
介绍：springboot项目打包成sh脚本形式启动，assembly插件打包自定义脚本参数

[详细介绍及代码详解](https://blog.csdn.net/qq_24950043/article/details/126555657)

## database_doc_demo
介绍：数据库说明文档自动生成器，支持html,word,html格式

[详细介绍及代码详解](https://blog.csdn.net/qq_24950043/article/details/128638072)

## file_view_demo
介绍：文档在线预览项目

[详细介绍及代码详解](https://wu55555.blog.csdn.net/article/details/128555073)

## mail_send_demo
介绍：
- java实现邮件及附件发送、HTML正文的三种方式（javax.mail、org.apache.commons.mail、spring-boot-starter-mail）
- 生成excel并作为附件邮件发送

[详细介绍及代码详解1](https://blog.csdn.net/qq_24950043/article/details/127156042)

[详细介绍及代码详解2](https://blog.csdn.net/qq_24950043/article/details/127160916)

[详细介绍及代码详解3](https://blog.csdn.net/qq_24950043/article/details/127171115)

[详细介绍及代码详解4](https://blog.csdn.net/qq_24950043/article/details/127171692)

## progress_bar_demo
介绍：基于springboot实现文件上传下载实时进度条功能

[详细介绍及代码详解](https://blog.csdn.net/qq_24950043/article/details/127478168)

## cpu_oom_demo
介绍：模拟线上OOM，CPU爆满问题，以实际问题为出发点，来理解JVM处理此类问题的思路及方法

[详细介绍及代码详解](https://wu55555.blog.csdn.net/article/details/127174391)

## local_cache_demo
介绍：本地缓存实现

[详细介绍及代码详解](https://blog.csdn.net/qq_24950043/article/details/130296868)

## powerjob-demo
介绍：分布式任务调度框架powerjob-demo

[详细介绍及代码详解](https://wu55555.blog.csdn.net/article/details/130175241)

## jetcache_demo
介绍：jetcache多级缓存框架demo

[详细介绍及代码详解](https://blog.csdn.net/qq_24950043/article/details/130478473)

## xxljob_demo
介绍：分布式任务调度框架xxl-job 客户端demo

[详细介绍及代码详解](https://wu55555.blog.csdn.net/article/details/129902454)

## solr_demo
介绍：solrJ,spring-data-solr集成演示

[详细介绍及代码详解](https://wu55555.blog.csdn.net/article/details/131259481)

[详细介绍及代码详解2](https://wu55555.blog.csdn.net/article/details/131148213)

## minio_demo
介绍：minio快速上手使用

[详细介绍及代码详解](https://wu55555.blog.csdn.net/article/details/128596068)

## easy-es-demo
介绍：easy-es组件快速上手

[详细介绍及代码详解](https://blog.csdn.net/qq_24950043/article/details/132127617)

## excel_import_demo
介绍：基于easy-poi实现的excel一对一、一对多、多对多的导入导出功能

[详细介绍及代码详解](https://blog.csdn.net/qq_24950043/article/details/138578553)

## encrypt_demo
介绍：基于RSA+AES实现前后端参数加密

## license_demo
介绍：基于trueLicense实现项目离线授权控制（程序部署到客户服务器，无需网络，控制程序使用有效期）

[详细介绍及代码详解](https://wu55555.blog.csdn.net/article/details/143624825)

## spi_demo
介绍：基于java spi机制实现热插拔插件

[详细介绍及代码详解](https://wu55555.blog.csdn.net/article/details/141634198)

## p7sign_demo
介绍：基于p7签名实现文件来源追溯和完整性校验

[详细介绍及代码详解](https://wu55555.blog.csdn.net/article/details/144374414)

## code_demo
介绍：链接生成二维码
[详细介绍及代码详解](https://wu55555.blog.csdn.net/article/details/131259497)

# 4.tool
## wu-core-common
介绍：核心工具包
