package com.example.webflux_demo.controller;

import com.example.webflux_demo.entity.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.List;

/**
 * @author benjamin_5
 * @Description
 * @date 2023/5/6
 */
@RestController
@RequestMapping("user")
public class UserController {

    @GetMapping("list")
    public Flux<User> list() throws InterruptedException {
        Thread.sleep(2000);
        User user1 = new User();
        user1.setId(1L);
        user1.setName("张三");
        user1.setAddress("北京");
        user1.setAge(18);
        Flux<User> list = Flux.just(user1, user1, user1);
        return list;
    }
}
