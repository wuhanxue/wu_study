package com.example.easyesdemo.entity;

import lombok.Data;
import org.dromara.easyes.annotation.IndexField;
import org.dromara.easyes.annotation.rely.FieldType;

/**
 * @author benjamin_5
 * @Description
 * @date 2023/8/6
 */
@Data
public class Product {

    @IndexField(fieldType = FieldType.KEYWORD)
    private String id;

    @IndexField(fieldType = FieldType.KEYWORD)
    private String name;

    private Double price;

    private Integer quantity;
}
