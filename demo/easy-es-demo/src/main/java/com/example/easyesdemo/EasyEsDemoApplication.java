package com.example.easyesdemo;

import org.dromara.easyes.starter.register.EsMapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EsMapperScan("com.example.easyesdemo.mapper")
public class EasyEsDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(EasyEsDemoApplication.class, args);
    }

}
