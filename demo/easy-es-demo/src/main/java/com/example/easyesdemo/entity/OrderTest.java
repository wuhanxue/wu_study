package com.example.easyesdemo.entity;

import lombok.Data;
import org.dromara.easyes.annotation.IndexField;
import org.dromara.easyes.annotation.IndexId;
import org.dromara.easyes.annotation.IndexName;
import org.dromara.easyes.annotation.rely.FieldType;

import java.util.Date;
import java.util.List;

/**
 * @author benjamin_5
 * @Description
 * @date 2023/8/6
 */
@Data
@IndexName(value = "order_test")
public class OrderTest {

    @IndexId
    private String id;

    private Integer status;

    private String no;

    private Double amount;

    @IndexField(fieldType = FieldType.NESTED)
    private List<Product> product;

    @IndexField(fieldType = FieldType.DATE, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    private String creator;

}
