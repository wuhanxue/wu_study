package com.example.easyesdemo.mapper;

import com.example.easyesdemo.entity.OrderTest;
import org.dromara.easyes.core.core.BaseEsMapper;

/**
 * @author benjamin_5
 * @Description
 * @date 2023/8/6
 */
public interface OrderTestEsMapper extends BaseEsMapper<OrderTest> {
}
