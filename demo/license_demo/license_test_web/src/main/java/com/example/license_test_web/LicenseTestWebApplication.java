package com.example.license_test_web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.example.*"})
public class LicenseTestWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(LicenseTestWebApplication.class, args);
    }

}
