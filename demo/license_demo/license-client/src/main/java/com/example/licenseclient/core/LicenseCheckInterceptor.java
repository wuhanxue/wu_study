package com.example.licenseclient.core;

import com.alibaba.fastjson.JSON;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * @author benjamin5
 * @Description 证书校验拦截器
 * @createTime 2024/10/31
 */
public class LicenseCheckInterceptor implements HandlerInterceptor {
    private static Logger logger = LogManager.getLogger(LicenseCheckInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        LicenseVerify licenseVerify = new LicenseVerify();

        //校验证书是否有效
        boolean verifyResult = licenseVerify.verify();

        if(verifyResult){
            return true;
        }else{
            response.setCharacterEncoding("UTF-8");
            response.setContentType("text/html; charset=UTF-8");
            Map<String,String> result = new HashMap<>(1);
            result.put("code","500");
            result.put("msg","您的许可证无效或过期，请重新申请！");

            response.getWriter().write(JSON.toJSONString(result));

            return false;
        }
    }

}
