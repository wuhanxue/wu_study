package com.example.licenseclient.config;

import com.example.licenseclient.core.LicenseCheckInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author benjamin_5
 * @Description 拦截器
 * @date 2024/11/1
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    /**
     * 添加拦截器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LicenseCheckInterceptor())
                .addPathPatterns("/**")
//                .excludePathPatterns("/license/generateLicense","/license/getServerInfos")
        ;
    }
}
