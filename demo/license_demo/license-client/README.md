# 证书生成
license证书生成及续期见dg-license-server服务

# 服务开启license校验
1、pom添加license-client依赖
```xml
 <dependency>
     <groupId>com.example</groupId>
     <artifactId>license-client</artifactId>
     <version>1.0.0</version>
</dependency>
```

2、服务中添加配置项
```yml
license:
  # 证书subject
  subject: license-test-web
  # 公钥别称
  publicAlias: publicCert
  # 访问公钥库的密码
  storePass: wu@2024555
  # 证书路径
  licensePath: /data/license/license.lic
  # 公钥路径
  publicKeysStorePath: /data/license/publicCerts.keystore
```

3、服务启动类添加license-client的包名，确保bean能够被spring扫描到
```java
@ComponentScan(basePackages = {"com.example.licenseclient"})
```