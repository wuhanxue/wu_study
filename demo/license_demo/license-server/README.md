# 说明
本项目为独立项目，与license-client配合用于license分发（服务部署到客户服务器时，离线控制可用授权期限）

注意本服务不要配置`@ComponentScan(basePackages = {"com.example.licenseclient"})`，防止扫描到`LicenseCheckListener`导致本服务也需要安装证书启动

# 证书生成
1、生成私钥库文件
```shell
keytool -genkeypair -keysize 1024 -validity 36500 -alias "privateKey" -keystore "privateKeys.keystore" -storepass "wu@2024555" -keypass "wu@private2024" -dname "CN=wu.com, OU=wu, O=wu, L=GUIZHOU, ST=GUIZHOU, C=CN"
```
参数：
- `-genkeypair` 生成一对非对称密钥
- `-keysize` 密钥长度
- `-validity` 有效期，单位天
- `-alias` 私钥别名
- `-keystore` 私钥文件名
- `-storepass` 访问整个私钥库的密码
- `-keypass` 别名对应私钥的密码
- `-dname` 证书持有者详细信息
> CN=localhost：CN是Common Name的缩写，通常用于指定域名或IP地址
OU=localhost：OU是Organizational Unit的缩写，表示组织单位
O=localhost：O是Organization的缩写，表示组织名
L=SH：L是Locality的缩写，表示城市或地区
ST=SH：ST是State的缩写，表示州或省
C=CN：C是Country的缩写，表示国家代码

2、导出别名为"privateKey"的密钥对的公钥
```shell
keytool -exportcert -alias "privateKey" -keystore "privateKeys.keystore" -storepass "wu@2024555" -file "certfile.cer"
```

3、将公钥文件导入公钥库
```shell
keytool -import -alias "publicCert" -file "certfile.cer" -keystore "publicCerts.keystore" -storepass "wu@2024555"
```

4、文件privateKeys.keystore用来为用户生成License文件，不可泄漏！

publicCerts.keystore随应用工程部署到客户服务器，用其解密License文件并校验其许可信息，提供给客户

certfile.cer文件为临时文件，可删除


5、通过接口生成`license/generateLicense`,如给dg-admin服务传参示例如下
```json
{
  "subject": "license-test-web",
  "storePass": "wu@2024555",
  "licensePath": "/Users/wuhanxue/Downloads/license/license.lic",
  "expiryTime": "2024-10-31 17:54:00",
  "description": "license-test-web模块分发证书"
}
```

生成的license.lic文件,提供给客户

# 历史客户license续期
通过接口生成`license/generateLicense`,如给dg-admin服务传参示例如下
```json
{
  "subject": "license-test-web",
  "storePass": "wu@2024555",
  "licensePath": "/Users/wuhanxue/Downloads/license/license.lic",
  "expiryTime": "2024-10-31 17:54:00",
  "description": "license-test-web模块分发证书"
}
```

生成的license.lic文件,提供给客户

# 客户服务端license部署说明

详见license-client模块