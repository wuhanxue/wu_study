package com.example.excel_import_demo.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author benjamin_5
 * @Description
 * @date 2024/5/8
 */
@Data
public class DataInfo {

    @Excel(name = "姓名" , needMerge = true)
    private String name;

    @Excel(name = "数量" , needMerge = true)
    private Integer number;

    @Excel(name = "地址" , needMerge = true)
    private String address;

    @Excel(name = "创建日期", format="yyyy-MM-dd", width = 24,needMerge = true)
    private Date createDate;
//
    @ExcelCollection(name = "订单信息")
    private List<DataInfoOrder> orderList;

    @ExcelCollection(name = "标签信息")
    private List<DataInfoTag> tagList;


}
