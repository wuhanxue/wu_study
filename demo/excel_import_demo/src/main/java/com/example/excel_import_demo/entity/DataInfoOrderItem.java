package com.example.excel_import_demo.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

/**
 * @author benjamin_5
 * @Description
 * @date 2024/12/12
 */
@Data
public class DataInfoOrderItem {

    @Excel(name = "商品分类名称")
    private String name;

    @Excel(name = "商品分类编码")
    private String value;
}
