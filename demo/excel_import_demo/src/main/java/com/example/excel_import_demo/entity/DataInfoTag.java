package com.example.excel_import_demo.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

/**
 * @author benjamin_5
 * @Description
 * @date 2024/5/8
 */
@Data
public class DataInfoTag {

    @Excel(name = "标签")
    private String tag;

    @Excel(name = "标签类型")
    private String type;

    @Excel(name = "等级")
    private Integer level;
}
