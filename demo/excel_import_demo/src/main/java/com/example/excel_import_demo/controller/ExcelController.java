package com.example.excel_import_demo.controller;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import com.example.excel_import_demo.entity.DataInfo;
import com.example.excel_import_demo.entity.DataInfoOrder;
import com.example.excel_import_demo.entity.DataInfoOrderItem;
import com.example.excel_import_demo.entity.DataInfoTag;
import com.example.excel_import_demo.util.ExcelUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * @author benjamin_5
 * @Description
 * @date 2024/5/8
 */
@RestController
@RequestMapping("excel")
public class ExcelController {

    @PostMapping("import")
    public List<DataInfo> importData(MultipartFile file) throws Exception {
        List<DataInfo> dataInfos = ExcelUtil.importExcel(file, 0, 1, DataInfo.class);
        return dataInfos;
    }

    @GetMapping("export")
    public void exportData(HttpServletResponse response){
        List<DataInfo> dataInfos = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            DataInfo info = new DataInfo();
            info.setName("数据"+i);
            info.setAddress("地址"+i);
            info.setNumber(i);
            info.setCreateDate(new Date());
            Random rand = new Random();
            int num = rand.nextInt(5) + 1;
            int num2 = rand.nextInt(5) + 1;
            List<DataInfoOrder> orderList = new ArrayList<>(num);
            for (int j = 1; j <= num; j++) {
                DataInfoOrder order = new DataInfoOrder();
                order.setPrice(new BigDecimal(j));
                order.setName("商品"+j);
                order.setOrderNo("订单号"+j);
                order.setType("类型"+j);

                DataInfoOrderItem item1 = new DataInfoOrderItem();
                item1.setName("分类"+j+"1");
                item1.setValue("code"+j+"1");

                DataInfoOrderItem item2 = new DataInfoOrderItem();
                item2.setName("分类"+j+"2");
                item2.setValue("code"+j+"2");

                List<DataInfoOrderItem> itemList = new ArrayList<>();
                itemList.add(item1);
                itemList.add(item2);

                order.setItemList(itemList);

                orderList.add(order);
            }
            List<DataInfoTag> tagList = new ArrayList<>();
            for (int j = 1; j <= num2; j++) {
                DataInfoTag tag = new DataInfoTag();
                tag.setTag("标签"+j);
                tag.setLevel(j);
                tag.setType("标签类型"+j);
                tagList.add(tag);
            }
            info.setOrderList(orderList);
            info.setTagList(tagList);
            dataInfos.add(info);
        }
        ExcelUtil.downLoadExcel("导出数据.xlsx", response, DataInfo.class, dataInfos);
    }


}
