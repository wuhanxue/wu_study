package com.example.excel_import_demo.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelCollection;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author benjamin_5
 * @Description
 * @date 2024/5/8
 */
@Data
public class DataInfoOrder {

    @Excel(name = "订单号")
    private String orderNo;

    @Excel(name = "价格")
    private BigDecimal price;

    @Excel(name = "商品类型")
    private String type;

    @Excel(name = "商品名称")
    private String name;

    @ExcelCollection(name = "商品分类")
    private List<DataInfoOrderItem> itemList;

}
