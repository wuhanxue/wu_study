package com.example.excel_import_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExcelImportDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExcelImportDemoApplication.class, args);
    }

}
