package com.wu.rocketmq_demo.demo;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.MessageQueueSelector;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageQueue;
import org.apache.rocketmq.remoting.common.RemotingHelper;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author benjamin_5
 * @Description 消息批量发送
 * @date 2024/6/14
 */
public class Producer4BatchDemo {

    public static void main(String[] args) throws Exception {
        try {
            DefaultMQProducer producer = new DefaultMQProducer("group_test");
            // 声明namesrv地址
            producer.setNamesrvAddr("localhost:9876");
            // 设置重试次数
            producer.setRetryTimesWhenSendFailed(2);
            producer.start();

            String topic = "topic_test";
            List<Message> messages = new ArrayList<>();
            messages.add(new Message(topic, "Tag",  "消息1".getBytes()));
            messages.add(new Message(topic, "Tag",  "消息2".getBytes()));
            messages.add(new Message(topic, "Tag",  "消息3".getBytes()));
            SendResult send = producer.send(messages);

            System.out.println("发送结果："+send);
            producer.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
