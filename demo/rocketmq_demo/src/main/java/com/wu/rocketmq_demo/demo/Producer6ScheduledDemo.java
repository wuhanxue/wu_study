package com.wu.rocketmq_demo.demo;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * @author benjamin_5
 * @Description 延迟消息发送
 * @date 2024/6/14
 */
public class Producer6ScheduledDemo {

    public static void main(String[] args) {
        try {
            DefaultMQProducer producer = new DefaultMQProducer("group_test");
            // 声明namesrv地址
            producer.setNamesrvAddr("localhost:9876");
            // 设置重试次数
            producer.setRetryTimesWhenSendFailed(2);
            producer.start();

            int totalMessagesToSend = 100;
            for (int i = 0; i < totalMessagesToSend; i++) {
                Message message = new Message("topic_test", ("延迟消息 " + i).getBytes());

                message.setDelayTimeLevel(3);
                // Send the message
                SendResult send = producer.send(message);
                System.out.println("发送结果："+send);
            }

            producer.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
