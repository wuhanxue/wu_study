package com.wu.rocketmq_demo.demo;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;

import java.nio.charset.StandardCharsets;

/**
 * @author benjamin_5
 * @Description 单向消息
 * @date 2024/6/14
 */
public class Producer5OneWayDemo {

    public static void main(String[] args) throws Exception{

        // 声明group
        DefaultMQProducer producer = new DefaultMQProducer("group_test");

        // 声明namesrv地址
        producer.setNamesrvAddr("localhost:9876");

        // 设置重试次数
        producer.setRetryTimesWhenSendFailed(2);
        // 启动实例
        producer.start();

        // 设置消息的topic,tag以及消息体
        Message msg = new Message("topic_test", "tag_test", "单向消息".getBytes(StandardCharsets.UTF_8));

        // 发送消息
        producer.sendOneway(msg);

        // 关闭实例
        producer.shutdown();
    }
}
