package com.example.image_compress_demo.util;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @author benjamin_5
 * @Description
 * @date 2023/7/26
 */
@Component
public class AsyUtil {

    @Async
    public void asy() throws InterruptedException {
        long start = System.currentTimeMillis();
        Thread.sleep(2000);
        long end = System.currentTimeMillis();
        System.out.println("同步耗时: "+(end-start));
    }
}
