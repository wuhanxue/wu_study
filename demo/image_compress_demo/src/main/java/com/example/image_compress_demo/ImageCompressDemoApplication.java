package com.example.image_compress_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class ImageCompressDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ImageCompressDemoApplication.class, args);
    }

}
