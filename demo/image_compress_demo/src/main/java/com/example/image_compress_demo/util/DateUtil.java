package com.example.image_compress_demo.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * 日期操作工具类
 *
 * @author Sum
 */
public class DateUtil {

    public final static SimpleDateFormat GMT_SF =
            new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss 'GMT'", Locale.ENGLISH);
    public final static SimpleDateFormat SOLR_SF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
    public final static SimpleDateFormat SOLR_DATE_FT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
    public final static int MILLS_OF_MINUTE = 60 * 1000;
    private final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat();

    public static String getCurTimeString(Format format) {
        DATE_FORMAT.applyPattern(format.value);
        return DATE_FORMAT.format(new Date());
    }

    public enum Format {
        NO_SYMBOL("yyyyMMddHHmmss", "(无)符号格式"),
        NO_SYMBOLS("yyyyMMddHHmmssSSS", "(无)符号格式(毫秒)"),
        SYMBOL("yyyy-MM-dd HH:mm:ss", "(有)符号格式"),
        SYMBOLS("yyyy-MM-dd HH:mm:ss.sss", "(有)符号格式(毫秒)");

        public final String value;
        public final String description;


        private Format(String value, String description) {
            this.value = value;
            this.description = description;
        }

    }


    public static String DEFAULT_FORMAT = "yyyy-MM-dd";

    public static String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";



}
