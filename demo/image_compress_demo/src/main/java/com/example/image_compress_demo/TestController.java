package com.example.image_compress_demo;

import com.example.image_compress_demo.util.AsyUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author benjamin_5
 * @Description
 * @date 2023/7/26
 */
@RestController
@RequestMapping("test")
public class TestController {

    @Autowired
    AsyUtil asyUtil;

    @GetMapping("thread")
    public void test() throws InterruptedException {
        long start = System.currentTimeMillis();
        asyUtil.asy();
        long end = System.currentTimeMillis();
        System.out.println("异步耗时: "+(end-start));
    }


}
