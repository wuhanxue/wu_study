package com.example.minio_demo;

import com.example.minio_demo.util.HttpsClientUtil;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;


import java.io.FileOutputStream;
import java.io.InputStream;

/**
 * @author benjamin_5
 * @Description
 * @date 2023/8/31
 */
public class DownloadTest {
    public static void main(String[] args) throws Exception {
        // Get API url
        String url = "http://localhost:8080/download/video";

        // Create HTTP client
        HttpClient client = HttpClientBuilder.create().build();

        // Make GET request
        HttpGet request = new HttpGet(url);

// Execute request
        HttpResponse response = client.execute(request);

// Get custom header
        Header header = response.getFirstHeader("X-sign");
        String value = header.getValue();
        System.out.println("X-sign: "+value);

// Get video file content
        HttpEntity entity = response.getEntity();
        InputStream videoStream = entity.getContent();

// Save video file
        FileOutputStream output = new FileOutputStream("/Users/wuhanxue/Downloads/video.mp4");
        byte[] buffer = new byte[4096];
        int bytesRead = -1;
        while ((bytesRead = videoStream.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }

// Close streams
        output.close();
        videoStream.close();
    }

}
