package com.example.minio_demo.util;


import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.mime.MultipartEntity;

import java.util.Map;

/**
 * @author Sum
 */
public class HttpParamBuilder {

    private final String url;
    private final HttpRequestBase request;
    private final Integer connectTimeout;
    private final Integer socketTimeout;
    private final Integer requestTimeout;
    private final Map<String, Object> params;
    private final Map<String, Object> headers;
    private final MultipartEntity entity;
    private final String jsonParam;
    private final String body;
    private final String contentType;
    private final String headerCharSet;
    private final String headerAccept;
    private final String proxyIp;
    private final byte[] bytes;
    private final Integer proxyPort;

    private HttpParamBuilder(Builder builder) {
        this.url = builder.url;
        this.request = builder.request;
        this.connectTimeout = builder.connectTimeout;
        this.socketTimeout = builder.socketTimeout;
        this.requestTimeout = builder.requestTimeout;
        this.params = builder.params;
        this.headers = builder.headers;
        this.entity = builder.entity;
        this.jsonParam = builder.jsonParam;
        this.body = builder.body;
        this.contentType = builder.contentType;
        this.headerCharSet = builder.headerCharSet;
        this.headerAccept = builder.headerAccept;
        this.proxyIp = builder.proxyIp;
        this.proxyPort = builder.proxyPort;
        this.bytes = builder.bytes;
    }

    public static Builder newBuilder(String url) {
        return new Builder(url);
    }

    public byte[] getBytes() {
        return bytes;
    }

    public String getProxyIp() {
        return proxyIp;
    }

    public Integer getProxyPort() {
        return proxyPort;
    }

    public Integer getRequestTimeout() {
        return requestTimeout;
    }

    public String getBody() {
        return body;
    }

    public String getHeaderAccept() {
        return headerAccept;
    }

    public String getHeaderCharSet() {
        return headerCharSet;
    }

    public String getContentType() {
        return contentType;
    }

    public String getJsonParam() {
        return jsonParam;
    }

    public MultipartEntity getEntity() {
        return entity;
    }

    public Map<String, Object> getHeaders() {
        return headers;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public String getUrl() {
        return url;
    }

    public HttpRequestBase getRequest() {
        return request;
    }

    public Integer getConnectTimeout() {
        return connectTimeout;
    }

    public Integer getSocketTimeout() {
        return socketTimeout;
    }

    public static class Builder {
        private String url;
        private HttpRequestBase request;
        private Integer connectTimeout;
        private Integer socketTimeout;
        private Integer requestTimeout;
        private Map<String, Object> params;
        private Map<String, Object> headers;
        private MultipartEntity entity;
        private String jsonParam;
        private String body;
        private String contentType;
        private String headerCharSet;
        private String headerAccept;
        private String proxyIp;
        private Integer proxyPort;
        private byte[] bytes;

        public Builder(String url) {
            this.url = url;
        }

        public Builder proxyIp(String proxyIp) {
            this.proxyIp = proxyIp;
            return this;
        }

        public Builder proxyPort(Integer proxyPort) {
            this.proxyPort = proxyPort;
            return this;
        }

        public Builder headerCharSet(String headerCharSet) {
            this.headerCharSet = headerCharSet;
            return this;
        }

        public Builder bytes(byte[] bytes) {
            this.bytes = bytes;
            return this;
        }

        public Builder headerAccept(String headerAccept) {
            this.headerAccept = headerAccept;
            return this;
        }

        public Builder body(String body) {
            this.body = body;
            return this;
        }

        public Builder contentType(String contentType) {
            this.contentType = contentType;
            return this;
        }

        public Builder params(Map<String, Object> params) {
            this.params = params;
            return this;
        }

        public Builder jsonParam(String jsonParam) {
            this.jsonParam = jsonParam;
            return this;
        }

        public Builder entity(MultipartEntity entity) {
            this.entity = entity;
            return this;
        }

        public Builder headers(Map<String, Object> headers) {
            this.headers = headers;
            return this;
        }

        public Builder request(HttpRequestBase request) {
            this.request = request;
            return this;
        }

        public Builder connectTimeout(Integer connectTimeout) {
            this.connectTimeout = connectTimeout;
            return this;
        }

        public Builder socketTimeout(Integer socketTimeout) {
            this.socketTimeout = socketTimeout;
            return this;
        }

        public Builder requestTimeout(Integer requestTimeout) {
            this.requestTimeout = requestTimeout;
            return this;
        }

        public HttpParamBuilder build() {
            return new HttpParamBuilder(this);
        }


    }
}
