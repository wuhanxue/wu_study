package com.example.minio_demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author benjamin_5
 * @Description 文件存储返回信息实体类
 * @date 2023/8/5
 */
@Data
@AllArgsConstructor
public class MinioReturn {

    /**
     * 文件地址
     */
    private String path;

    /**
     * 原始文件名
     */
    private String inputName;

    /**
     * 最终文件名
     */
    private String outPutName;

}
