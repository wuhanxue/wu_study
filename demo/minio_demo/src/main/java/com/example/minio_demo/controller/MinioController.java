package com.example.minio_demo.controller;

import com.example.minio_demo.entity.MinioReturn;
import com.example.minio_demo.template.MinioTemplate;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @author benjamin_5
 * @Description
 * @date 2023/8/5
 */
@RestController
@RequestMapping("minio")
@AllArgsConstructor
public class MinioController {

    private final MinioTemplate minioTemplate;

    @PostMapping("/upload")
    @ResponseBody
    public MinioReturn upload(MultipartFile file) throws Exception {
        return minioTemplate.putFile(file);
    }

    @PostMapping("/remove")
    @ResponseBody
    public String remove(String fileName, String bucketName) throws Exception{
        minioTemplate.removeFile(bucketName, fileName);
        return "success";
    }

}
