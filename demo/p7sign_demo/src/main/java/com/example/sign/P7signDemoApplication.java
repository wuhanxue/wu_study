package com.example.sign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class P7signDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(P7signDemoApplication.class, args);
    }

}
