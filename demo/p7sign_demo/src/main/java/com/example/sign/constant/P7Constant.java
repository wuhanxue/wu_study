package com.example.sign.constant;

/**
 * @author benjamin_5
 * @Description
 */
public interface P7Constant extends P7KeyConstant {
    /**
     * 签名身份
     * CN: CN是Common Name的缩写，通常用于指定域名或IP地址
     * O: O是Organization的缩写，表示组织名
     * L: L是Locality的缩写，表示城市或地区
     * ST: ST是State的缩写，表示州或省
     * C: C是Country的缩写，表示国家代码
     */
    String DN = "CN=wu.com, O=, L=贵阳, ST=贵州, C=中国";

    /**
     * 签名有效期:100年
     */
    long VALIDITY_MILLISECOND = 36500L * 24 * 60 * 60 * 1000;
    /**
     * 证书前缀 x.509语法
     */
    String P7_CERT_BEGIN_STR = "-----BEGIN CERTIFICATE-----\n";
    /**
     * 证书后缀 x.509语法
     */
    String P7_CERT_END_STR = "\n-----END CERTIFICATE-----";
}
