package com.example.sign.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author benjamin_5
 * @Description hash值计算工具
 */
public class HashUtil {

    public static String hashFile(byte[] fileData) throws NoSuchAlgorithmException {
        byte[] digest = getDigest(fileData);
        return byte2hexLower(digest);
    }

    public static String hashFile(String path) throws Exception {
        return hashFile(new File(path));
    }

    /**
     * 计算文件hash值
     */
    public static String hashFile(File file) throws Exception {
        FileInputStream fis = null;
        String sha256 = null;
        try {
            fis = new FileInputStream(file);
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] buffer = new byte[1024];
            int length = -1;
            while ((length = fis.read(buffer, 0, 1024)) != -1) {
                md.update(buffer, 0, length);
            }
            byte[] digest = md.digest();
            sha256 = byte2hexLower(digest);
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("计算文件hash值错误");
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sha256;
    }

    public static String byte2hexLower(byte[] b) {
        String hs = "";
        String stmp = "";
        for (int i = 0; i < b.length; i++) {
            stmp = Integer.toHexString(b[i] & 0XFF);
            if (stmp.length() == 1) {
                hs = hs + "0" + stmp;
            } else {
                hs = hs + stmp;
            }
        }
        return hs;
    }

    /**
     * 生成数据摘要
     * @param bytes
     * @return
     * @throws NoSuchAlgorithmException
     */
    public static byte[] getDigest(byte[] bytes) throws NoSuchAlgorithmException {
        // 创建消息摘要对象
        MessageDigest digest = MessageDigest.getInstance("SHA-256");
        // 计算摘要
        return digest.digest(bytes);
    }
}
