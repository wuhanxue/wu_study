package com.example.sign;

import java.util.Map;

import static com.example.sign.core.P7KeyGenerator.*;

/**
 * @author benjamin_5
 * @Description
 * @date 2023/9/13
 */
public class P7KeyGeneratorTest {
    public static void main(String[] args) throws Exception {
        Map<String, String> keys = generateKeys();
        System.out.println("P7签名私钥："+keys.get(PRIVATE_KEY));
        System.out.println("P7签名公钥："+keys.get(PUBLIC_KEY));
        System.out.println("P7公钥证书："+keys.get(PUBLIC_CERT_KEY));
    }
}
