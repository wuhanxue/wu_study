package com.example.sign;


import com.example.sign.util.HashUtil;
import com.example.sign.util.P7SignUtil;

import static com.example.sign.constant.P7KeyConstant.PUBLIC_CERT;
import static com.example.sign.util.P7SignUtil.createSign;
import static com.example.sign.util.P7SignUtil.verifySign;

/**
 * @author benjamin_5
 * @Description
 * @date 2023/9/14
 */
public class P7UtilTest {

    public static void main(String[] args) throws Exception {
        String file = "/Users/wuhanxue/Downloads/企业数据V1.0.xlsx";
        String file2 = "/Users/wuhanxue/Downloads/企业数据V2.0.xlsx"; // 修改后文件
        String hash = HashUtil.hashFile(file);
        String hash2 = HashUtil.hashFile(file2);

        String sign = createSign(hash.getBytes());
        System.out.println("签名："+sign);

        P7SignUtil.SignReturn signReturn = verifySign(hash2.getBytes(), sign, PUBLIC_CERT);
        System.out.println(signReturn);
    }
}
