package com.example.file;

import java.io.InputStream;

/**
 * @author benjamin_5
 * @Description
 * @date 2024/8/28
 */
public class ObsService implements IFileService{

    @Override
    public String type() {
        return "obs";
    }

    @Override
    public String makeBucket(String bucketName) {
        return  "obs create " + bucketName + " bucket success";
    }

    @Override
    public boolean existBucket(String bucketName) {
        return false;
    }

    @Override
    public boolean removeBucket(String bucketName) {
        return false;
    }

    @Override
    public boolean setBucketExpires(String bucketName, int days) {
        return false;
    }

    @Override
    public void upload(String bucketName, String fileName, InputStream stream) {

    }
}
