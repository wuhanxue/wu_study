package com.example.file;

import java.io.InputStream;

/**
 * @author benjamin_5
 * @Description
 * @date 2024/8/28
 */
public interface IFileService {
    String type();

    String makeBucket(String bucketName);

    boolean existBucket(String bucketName);

    boolean removeBucket(String bucketName);

    boolean setBucketExpires(String bucketName, int days);

    void upload(String bucketName, String fileName, InputStream stream);
}
