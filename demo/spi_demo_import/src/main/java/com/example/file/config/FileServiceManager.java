package com.example.file.config;

import com.example.file.IFileService;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author benjamin_5
 * @Description
 * @date 2024/8/28
 */
public class FileServiceManager {

    private static final FileServiceManager MANAGER = new FileServiceManager();

    private final Map<String, IFileService> SERVICE_MAP = new HashMap<>();

    public FileServiceManager() {
        init();
    }

    private void init(){
        // FileServiceLoader通过反射加载和实例化
        System.out.println("初始化...");
        Collection<IFileService> authPluginServices = FileServiceLoader.load(IFileService.class);
        for (IFileService service : authPluginServices) {
            System.out.println(service.type()+"初始化成功");
            SERVICE_MAP.put(service.type(), service);
        }
    }

    /**
     * 获取单例
     * @return
     */
    public static FileServiceManager getInstance(){
        return MANAGER;
    }

    /**
     * 获取对应的实现类
     * @param type
     * @return
     */
    public IFileService getService(String type){
        return SERVICE_MAP.get(type);
    }

}
