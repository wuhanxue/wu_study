package com.example.spi_demo_extend.service;

import com.example.file.IFileService;

import java.io.InputStream;

/**
 * @author benjamin_5
 * @Description
 * @date 2024/8/28
 */
public class FtpService implements IFileService {

    @Override
    public String type() {
        return "ftp";
    }

    @Override
    public String makeBucket(String bucketName) {
        return  "ftp create " + bucketName + " bucket success";
    }

    @Override
    public boolean existBucket(String bucketName) {
        return false;
    }

    @Override
    public boolean removeBucket(String bucketName) {
        return false;
    }

    @Override
    public boolean setBucketExpires(String bucketName, int days) {
        return false;
    }

    @Override
    public void upload(String bucketName, String fileName, InputStream stream) {

    }
}
