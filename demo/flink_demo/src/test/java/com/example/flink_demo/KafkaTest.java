package com.example.flink_demo;


import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.junit.jupiter.api.Test;

import java.util.Properties;

/**
 * @author benjamin_5
 * @Description
 * @date 2024/9/25
 */
public class KafkaTest {

    @Test
    public void send() {
        Properties properties = new Properties();
        //连接kafka集群 bootstrap-server
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        //指定key和value序列化器
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        //2、创建kafka producer生产者
        KafkaProducer<String, String> stringStringKafkaProducer = new KafkaProducer<String, String>(properties);

        //3、指定topic并发送数据
        stringStringKafkaProducer.send(new ProducerRecord<>("flink_test", "hello world"));
        stringStringKafkaProducer.send(new ProducerRecord<>("flink_test", "test world"));
        stringStringKafkaProducer.send(new ProducerRecord<>("flink_test", "good world"));

        //4、关闭资源
        stringStringKafkaProducer.close();

    }


}
