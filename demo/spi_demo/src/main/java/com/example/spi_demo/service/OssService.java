package com.example.spi_demo.service;

import com.example.file.IFileService;

import java.io.InputStream;

/**
 * @author benjamin_5
 * @Description
 * @date 2024/8/28
 */
public class OssService implements IFileService {

    @Override
    public String type() {
        return "oss";
    }

    @Override
    public String makeBucket(String bucketName) {
        return "oss create " + bucketName + " bucket success";
    }

    @Override
    public boolean existBucket(String bucketName) {
        return false;
    }

    @Override
    public boolean removeBucket(String bucketName) {
        return false;
    }

    @Override
    public boolean setBucketExpires(String bucketName, int days) {
        return false;
    }

    @Override
    public void upload(String bucketName, String fileName, InputStream stream) {

    }
}
