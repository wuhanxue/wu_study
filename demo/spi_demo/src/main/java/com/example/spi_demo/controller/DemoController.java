package com.example.spi_demo.controller;

import com.example.file.IFileService;
import com.example.file.ObsService;
import com.example.file.config.FileServiceManager;
import com.example.spi_demo.service.MinioService;
import com.example.spi_demo.service.OssService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ServiceLoader;

/**
 * @author benjamin_5
 * @Description
 * @date 2024/8/28
 */
@RestController
public class DemoController {
    @Value("${file.service.type}")
    private String defaultType;

    @GetMapping("createBucket")
    public String createBucket(String name, Integer type){
        ServiceLoader<IFileService> serviceLoader = ServiceLoader.load(IFileService.class);
        IFileService fileService = null;
        for (IFileService service: serviceLoader){
            if(type == 0){
                if(service instanceof MinioService){
                    fileService = service;
                }
            }else if(type == 1){
                if(service instanceof OssService){
                    fileService = service;
                }
            }else {
                if(service instanceof ObsService){
                    fileService = service;
                }
            }
        }
        return fileService.makeBucket(name);
    }

    @GetMapping("create")
    public String create(String name, String type){
        if(type == null || type.length() == 0){
            type = defaultType;
        }
        IFileService service = FileServiceManager.getInstance().getService(type);
        return service.makeBucket(name);
    }
}
