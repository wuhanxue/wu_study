package com.example.solrdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.solr.repository.config.EnableSolrRepositories;

@SpringBootApplication
@EnableSolrRepositories(basePackages = "com.example.solrdemo.dao")
public class SolrDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SolrDemoApplication.class, args);
    }

}
