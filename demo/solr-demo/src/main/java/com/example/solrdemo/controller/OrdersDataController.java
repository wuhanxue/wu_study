package com.example.solrdemo.controller;

import com.example.solrdemo.entity.Orders;
import com.example.solrdemo.dao.OrdersRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.solr.core.SolrTemplate;
import org.springframework.data.solr.core.query.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author benjamin_5
 * @Description
 * @date 2023/6/24
 */
@RestController
@RequestMapping("orders/data")
public class OrdersDataController {

    @Autowired
    private SolrTemplate solrTemplate;
    @Autowired
    private OrdersRepository ordersRepository;

    @GetMapping("save")
    public void save(Long id){
        Orders orders = new Orders();
        orders.setId(id);
        orders.setOrderNo("20230601000"+id);
        orders.setAddress("贵州省遵义市");
        orders.setProductName("小米手机");
        orders.setRemarks("送货上门");
        orders.setCreateUser("root");
        orders.setCreateTime(new Date());
        orders.setStatus(1);
        ordersRepository.save(orders);
    }

    @GetMapping("searchByName")
    public List<Orders> searchByName(String productName){
        return ordersRepository.findByProductName(productName);
    }

    @GetMapping("searchByAddressAndName")
    public List<Orders> search2(String address, String name){
        return ordersRepository.findByAddressOrProductName(address, name);
    }

    @GetMapping("save2")
    public void save2(Long id){
        Orders orders = new Orders();
        orders.setId(id);
        orders.setOrderNo("20230601000"+id);
        orders.setAddress("贵州省遵义市");
        orders.setProductName("小米手机");
        orders.setRemarks("送货上门");
        orders.setCreateUser("root");
        orders.setCreateTime(new Date());
        orders.setStatus(1);
        solrTemplate.saveBean("orders", orders);
        solrTemplate.commit("orders");
    }

    @GetMapping("search3")
    public Page<Orders> search3(String address, String name){

        Query query = Query.query("address:"+address+" AND product_name:"+name);

        Page<Orders> orders = solrTemplate.query("orders", query, Orders.class);
        return orders;
    }

}
