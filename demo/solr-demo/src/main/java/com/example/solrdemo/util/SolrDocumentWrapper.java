package com.example.solrdemo.util;

import org.apache.solr.common.SolrInputDocument;

import java.lang.reflect.Field;

/**
 * @author benjamin_5
 * @Description
 * @date 2023/6/11
 */
public class SolrDocumentWrapper {

    public static SolrInputDocument convertBeanToSolrInputDocument(Object bean) throws IllegalAccessException {
        // 创建 SolrInputDocument 对象
        SolrInputDocument doc = new SolrInputDocument();
        // 获取指定 Java Bean 对象的所有属性列表
        Field[] fields = bean.getClass().getDeclaredFields();
        // 枚举所有字段，并将其添加到 SolrInputDocument 对象中
        for (Field field : fields) {
            // 设置可访问性（如果不设置，则无法获取私有字段）
            field.setAccessible(true);
            // 获取字段名称和值
            String fieldName = field.getName();
            org.apache.solr.client.solrj.beans.Field fieldSolr = field.getDeclaredAnnotation(org.apache.solr.client.solrj.beans.Field.class);
            Object fieldValue = field.get(bean);
            // 如果字段的值不为空，则将其添加到 SolrInputDocument 中
            if (fieldValue != null) {
                doc.setField(fieldSolr != null ? fieldSolr.value() : fieldName, fieldValue);
            }
        }
        return doc;
    }
}
