package com.example.solrdemo.prop;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author benjamin_5
 * @Description
 * @date 2023/6/11
 */
@ConfigurationProperties(prefix = "solr")
@Component
@Data
public class SolrProperties {

    private String host;

    private Integer connectionTimeout;
    private Integer socketTimeout;

}
