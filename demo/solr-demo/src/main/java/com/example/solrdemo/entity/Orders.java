package com.example.solrdemo.entity;

import lombok.Data;
import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.SolrDocument;

import java.util.Date;
import java.util.List;

/**
 * @author benjamin_5
 * @Description
 * @date 2023/6/11
 */
@Data
@SolrDocument(collection = "orders")
public class Orders {

    @Field("id")
    @Id
    @Indexed(name = "id", type = "long")
    private Long id;

    @Field("order_no")
    @Indexed(name = "order_no", type = "string")
    private String orderNo;

    @Field("address")
    @Indexed(name = "address", type = "text_general")
    private String address;

    @Field("product_name")
    @Indexed(name = "product_name", type = "string")
    private String productName;

    @Field("remarks")
    @Indexed(name = "remarks", type = "string")
    private String remarks;

    @Field("status")
    @Indexed(name = "status", type = "int")
    private Integer status;

    @Field("create_user")
    @Indexed(name = "create_user", type = "string")
    private String createUser;

    @Field("create_time")
    @Indexed(name = "create_time", type = "date")
    private Date createTime;

    @Field("labels")
    @Indexed(name = "labels", type = "string")
    private List<String> labels;

}
