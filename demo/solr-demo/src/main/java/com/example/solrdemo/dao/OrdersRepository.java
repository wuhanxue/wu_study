package com.example.solrdemo.dao;

import com.example.solrdemo.entity.Orders;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.solr.repository.SolrCrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author benjamin_5
 * @Description
 * @date 2023/6/24
 */
@Repository
public interface OrdersRepository extends SolrCrudRepository<Orders, Long> {

    List<Orders> findByAddressOrProductName(String address, String productName);

    List<Orders> findByProductName(String productName);
}
