package com.example.solrdemo.controller;

import com.alibaba.fastjson.JSONObject;
import com.example.solrdemo.entity.Orders;
import lombok.AllArgsConstructor;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;


/**
 * @author benjamin_5
 * @Description
 * @date 2023/6/11
 */
@RestController
@RequestMapping("orders")
@AllArgsConstructor
public class OrdersController {
    private final HttpSolrClient httpSolrClient;

    @GetMapping("search")
    public List<Orders> search(String productName, String address, String remarks, String labels) throws SolrServerException, IOException {
        SolrQuery query = new SolrQuery();
        if (!StringUtils.isEmpty(productName)) {
            query.setQuery("product_name:" + productName);
        }
        if (!StringUtils.isEmpty(address)) {
            query.setQuery("address:" + address);
        }
        if (!StringUtils.isEmpty(remarks)) {
            query.setQuery("remarks:" + remarks);
        }
        if (!StringUtils.isEmpty(labels)) {
            query.setQuery("labels:" + labels);
        }
        if(StringUtils.isEmpty(query.getQuery())){
            query.setQuery("*:*");
        }
        query.setStart(0);
        query.setRows(5);
        QueryResponse response = httpSolrClient.query("orders",query);
        List<Orders> list = response.getBeans(Orders.class);
        return list;
    }

    @GetMapping("search2")
    public String search2(String productName, String address, String remarks, String labels) throws SolrServerException, IOException {
        SolrQuery query = new SolrQuery();
        if (!StringUtils.isEmpty(productName)) {
            query.setQuery("product_name:" + productName);
        }
        if (!StringUtils.isEmpty(address)) {
            query.setQuery("address:" + address);
        }
        if (!StringUtils.isEmpty(remarks)) {
            query.setQuery("remarks:" + remarks);
        }
        if (!StringUtils.isEmpty(labels)) {
            query.setQuery("labels:" + labels);
        }
        if(StringUtils.isEmpty(query.getQuery())){
            query.setQuery("*:*");
        }
        // 开启查询结果高亮
        query.setHighlight(true);
        query.addHighlightField("labels");
//        query.addHighlightField("product_name");
//        query.addHighlightField("address");
//        query.addHighlightField("remarks");
        query.setHighlightSimplePre("<div style='color:red'>");
        query.setHighlightSimplePost("</div>");

        query.setStart(0);
        query.setRows(5);
        QueryResponse response = httpSolrClient.query("orders",query);
        List<Orders> list = response.getBeans(Orders.class);
        Map<String, Map<String, List<String>>> highlighting = response.getHighlighting();

        for (Orders orders : list) {
            Map<String, List<String>> map = highlighting.get(orders.getId());
            List<String> labels1 = map.get("labels");
            System.out.println(labels1);
        }
        return JSONObject.toJSONString(highlighting);
    }

}
