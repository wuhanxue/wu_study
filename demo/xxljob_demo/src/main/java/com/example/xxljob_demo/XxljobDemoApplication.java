package com.example.xxljob_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XxljobDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(XxljobDemoApplication.class, args);
    }

}
