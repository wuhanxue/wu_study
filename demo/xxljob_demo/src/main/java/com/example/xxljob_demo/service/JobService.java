package com.example.xxljob_demo.service;

import cn.hutool.http.HttpUtil;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * @author benjamin_5
 * @Description
 * @date 2023/6/7
 */
@Component
public class JobService {

    @XxlJob("testJob")
    public void testJob(){
        String param = XxlJobHelper.getJobParam();
        System.out.println("执行定时任务，参数为：" + param);
    }

    @XxlJob("orderSolrImport")
    public void solrImport(){
        HashMap<String, Object> param = new HashMap<>();
        param.put("qt", "/dataimport");
        param.put("command", "delta-import");
        String result = HttpUtil.get("http://192.168.244.41:8983/solr/orders/dataimport", param);
        System.out.println("同步成功：\n"+result);
    }

}
