package com.example.netty_demo.controller;

import com.example.netty_demo.service.NettyClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author benjamin_5
 * @Description
 * @date 2024/8/30
 */
@RestController
public class DemoController {

    @Autowired
    private NettyClient nettyClient;

    @GetMapping("send")
    public String send(String msg){
        try {
            nettyClient.start();
            boolean res = nettyClient.send(msg);
            return res ? "发送成功" : "发送失败";
        } catch (InterruptedException e) {
            e.printStackTrace();
            return "发送失败";
        }
        finally {
            try {
                nettyClient.stop();
            }catch (Exception e){
                System.out.println("netty client close error："+e.getClass());
                e.printStackTrace();
            }
        }
    }
}
