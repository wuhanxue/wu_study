package com.example.encrypt_demo;

import com.example.encrypt_demo.util.AesUtil;
import com.example.encrypt_demo.util.RandomUtil;
import com.example.encrypt_demo.util.RsaUtil;

/**
 * @author benjamin_5
 * @Description
 * @date 2024/12/2
 */
public class Test {

    public static void main(String[] args) throws Exception {
        String key = "123asdasdgasdasd";

        String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCyT/Yi31QTd4BiUGEbC9R2wLD4lRVFsaytZtHcfyxQBlqqnuHyVXRi3lXcdRqNun/uDmZyrC6DZmVzpejydZkmoPc0gvw4fjC16wf0IXZFEEcwKbZyWe8uvrd9hYOEBUGGfyudFnX73QlY9ojMAUCNX/VJE2Y+uZGnvBdkZTQPdwIDAQAB";
        String privateKey = "MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBALJP9iLfVBN3gGJQYRsL1HbAsPiVFUWxrK1m0dx/LFAGWqqe4fJVdGLeVdx1Go26f+4OZnKsLoNmZXOl6PJ1mSag9zSC/Dh+MLXrB/QhdkUQRzAptnJZ7y6+t32Fg4QFQYZ/K50WdfvdCVj2iMwBQI1f9UkTZj65kae8F2RlNA93AgMBAAECgYAf65iQXZJbecUN1H3Nrax3Jb+IgIeyVkAnOoTPCCUSnlCUwqSIESwolsygDoaWYJCOOR214YTiGO6IaWbuIIctypnPB4FJHbHTYpIFhifArctplTyfBdDP28i8EveYq3ZTXfuimJdwWk/htKQm8QF6Qm//1m5aI/f6DbZjHG4C4QJBANkOZ6WILC7OsLXnx6gqx64mHtwecQuos74QXMIkQDhHoSO3V8BlIqpCRbUt+bmM11zZhv/DJd8IuVLyXMAvtA0CQQDSTgM2pjjYs5smhM79gA8y6EKah2ptnQdAtJKWFGJewpWS/NsFX0uAGt29V8aPFw3+sewBskWUOeV4lYTrx1yTAkBFcSVZwRoZk4jXeaLXu3AM2CdRVu/lwgBXU1bJyd095DS6f4K0i+auP+ubTm0xnsyb6QAWvXFVTK41ylJALkLVAkBga1H/2T/Q4q+sflFkWjUdsuzuak16SfdMTD20v03GglIPXQyRrvovlqG5MKghbIGHF03MDU7nQZAas/qIGxQvAkBiVVfVNqkRZ6QRnleofE8CGeWxJI4N0vpvxfFR1EaQOFP+FXepXuilNFZ7E66TiBEfjgKdtXRpi8IV4O41J6e0";

        for (int i = 0; i < 100; i++) {
            String data = RandomUtil.generateRandomString(1999);

            long start = System.currentTimeMillis();
            String s = AesUtil.aesEncrypt(data, key);
            long end = System.currentTimeMillis();
            System.out.println("aes耗时："+(end-start));
            long start1 = System.currentTimeMillis();
            String s1 = RsaUtil.rsaEncrypt(data, publicKey);
            long end1 = System.currentTimeMillis();
            System.out.println("rsa耗时："+(end1-start1));
        }


    }
}
