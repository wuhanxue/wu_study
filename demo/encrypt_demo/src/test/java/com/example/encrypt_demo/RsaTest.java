package com.example.encrypt_demo;

import com.example.encrypt_demo.util.RsaUtil;

/**
 * @author benjamin_5
 * @Description
 * @date 2024/9/9
 */
public class RsaTest {

    public static void main(String[] args) throws Exception {
        String decrypt = RsaUtil.rsaDecrypt("BN9UCa45qIu0Yo/ZCab3yN9Cdgiwi00v0DsOJOj53PAjmh48J82TVMv/47BJIW65uWjoahqlZ+iVlRaHRC+CUEPLy/jQusmU8BULdkT2kK26ZVOmEsDAbcIam9hVsACOMVkxPdV30g3qpOi773RAcjbgvxLaAZCQ3FSOij7mP8s=", RsaUtil.PRIVATE_KEY);
        System.out.println("解密: "+decrypt);
    }
}
