package com.example.encrypt_demo.entity;

import lombok.Data;

/**
 * @author benjamin_5
 * @Description
 * @date 2024/9/9
 */
@Data
public class R<T> {

    private T data;

    private int code;

    private String message;

    public static <T> R<T> success(T data){
        R<T> res = new R<>();
        res.setCode(200);
        res.setData(data);
        res.setMessage("操作成功");
        return res;
    }

    public static <T> R<T> fail(String message){
        R<T> res = new R<>();
        res.setCode(500);
        res.setMessage(message);
        return res;
    }
}
