package com.example.encrypt_demo.util;


import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Stack;

/**
 * AES加密
 */
public class AesUtil {

    /**
     * 算法/模式/补位(代码中实际为zeroPadding)
     */
    private static final String AES_MODE = "AES/CBC/NoPadding";
    private static final String MODE = "AES";

    /**
     * AES加密
     *
     * @param content    待加密的内容
     * @param encryptKey 加密密钥
     * @return 加密后的byte[]
     * @throws Exception
     */
    private static byte[] aesEncryptToBytes(String content, String encryptKey) throws Exception {
        Cipher cipher = Cipher.getInstance(AES_MODE);
        int blockSize = cipher.getBlockSize();
        byte[] dataBytes = content.getBytes(StandardCharsets.UTF_8);
        // zeroPadding
        int plaintextLength = dataBytes.length;
        if (plaintextLength % blockSize != 0) {
            plaintextLength = plaintextLength + (blockSize - (plaintextLength % blockSize));
        }
        byte[] plaintext = new byte[plaintextLength];
        System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);

        SecretKeySpec keySpec = new SecretKeySpec(encryptKey.getBytes(), MODE);
        IvParameterSpec ivSpec = new IvParameterSpec(encryptKey.getBytes());
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);
        return cipher.doFinal(plaintext);
    }

    /**
     * AES加密为base 64 code
     *
     * @param content    待加密的内容
     * @param encryptKey 加密密钥
     * @return 加密后的base 64 code
     * @throws Exception
     */
    public static String aesEncrypt(String content, String encryptKey) throws Exception {
        try {
            return Base64.getEncoder().encodeToString(aesEncryptToBytes(content, encryptKey));
        } catch (Exception e) {
            throw new Exception("加密失败！");
        }
    }

    /**
     * AES解密
     *
     * @param encryptBytes 待解密的byte[]
     * @param decryptKey   解密密钥
     * @return 解密后的String
     * @throws Exception
     */
    private static String aesDecryptByBytes(byte[] encryptBytes, String decryptKey) throws Exception {

        Cipher cipher = Cipher.getInstance(AES_MODE);
        SecretKeySpec keySpec = new SecretKeySpec(decryptKey.getBytes(), MODE);
        IvParameterSpec ivSpec = new IvParameterSpec(decryptKey.getBytes());

        cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
        byte[] decryptBytes = cipher.doFinal(encryptBytes);
        String result = new String(decryptBytes);
        if (!"".equals(result)) {
            result = result.trim();
        }
        return result;
    }

    /**
     * 将base 64 code AES解密
     *
     * @param encryptStr 待解密的base 64 code
     * @param decryptKey 解密密钥
     * @return 解密后的string
     * @throws Exception
     */
    public static String aesDecrypt(String encryptStr, String decryptKey) throws Exception {
        try {
            return (encryptStr == null || "".equals(encryptStr)) ? null : aesDecryptByBytes(base64Decode(encryptStr), decryptKey);
        } catch (Exception e) {
            throw new Exception("解密失败！");
        }
    }

    /**
     * base 64 decode
     *
     * @param base64Code 待解码的base 64 code
     * @return 解码后的byte[]
     * @throws Exception
     */
    private static byte[] base64Decode(String base64Code) throws Exception {
        return (base64Code == null || "".equals(base64Code)) ? null : Base64.getMimeDecoder().decode(base64Code);
    }
}
