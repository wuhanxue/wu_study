package com.example.encrypt_demo.filter;

import com.example.encrypt_demo.util.AesUtil;
import com.example.encrypt_demo.util.RsaUtil;
import com.sun.tools.javac.util.StringUtils;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Map;

/**
 * @author benjamin_5
 * @Description 参数解密过滤器
 * @date 2024/9/9
 */
@Component
@WebFilter(filterName = "decryptFilter", urlPatterns = "/*")
public class DecryptParamsFilter implements Filter {


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);
    }

    @Override
    public void destroy() {
        Filter.super.destroy();
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        // 过滤url
//        String requestURI = httpRequest.getRequestURI();
//        if(requestURI != null && requestURI.contains("xx/xx")){
//            chain.doFilter(request, response);
//            return;
//        }
        String keyEncrypt = httpRequest.getHeader(DecryptParamsWrapper.KEY_NAME);
        if(keyEncrypt == null || keyEncrypt.length() == 0){
            chain.doFilter(request, response);
            return;
        }
//        String sign = httpRequest.getHeader(DecryptParamsWrapper.SIGN_NAME);
//        Map<String, String[]> parameterMap = httpRequest.getParameterMap();
//        if(!RsaUtil.verify(parameterMap, RsaUtil.SIGN_PUBLIC_KEY ,sign)){
//
//            throw new RuntimeException("签名不合法");
//        }

        DecryptParamsWrapper wrapper = new DecryptParamsWrapper(httpRequest);
        chain.doFilter(wrapper, response);
    }
}
