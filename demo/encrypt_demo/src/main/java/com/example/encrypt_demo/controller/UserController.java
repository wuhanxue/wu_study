package com.example.encrypt_demo.controller;

import com.example.encrypt_demo.util.AesUtil;
import com.example.encrypt_demo.util.RsaUtil;
import com.example.encrypt_demo.entity.R;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author benjamin_5
 * @Description
 * @date 2024/9/9
 */
@RestController
@RequestMapping("user")
public class UserController {

    @PostMapping("add")
    public R addUser(String userName, HttpServletRequest request){
        // 模拟操作成功, 将解密的参数返回
        try {
//            String keyEncrypt = request.getHeader("encryptKey");
//            String key = RsaUtil.rsaDecrypt(keyEncrypt, RsaUtil.privateKey);
//            System.out.println("密钥："+key);
//            String decrypt = AesUtil.aesDecrypt(userName, key);
//            System.out.println("参数："+decrypt);
//            return R.success(decrypt);
            return R.success(userName);
        } catch (Exception e) {
            e.printStackTrace();
            return R.fail("解密失败");
        }
    }
}
